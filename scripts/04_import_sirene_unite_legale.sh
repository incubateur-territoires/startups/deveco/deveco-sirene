#!/bin/sh
set -e

SOURCE_URL="https://www.data.gouv.fr/fr/datasets/r/825f4199-cadd-486c-ac46-a65a8ea1a047"
OUTPUT_FILE="$DATA_DIR/sirene_unite_legale.zip"
DB_TABLE="sirene_unite_legale"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START IMPORTING $DB_TABLE"

if [ ! -f "$OUTPUT_FILE" ]; then
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD $DB_TABLE"
	wget --no-clobber --progress=bar:force:noscroll -q --show-progress $SOURCE_URL -O "$OUTPUT_FILE"
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD FINISHED $DB_TABLE"
fi

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START COPYING $DB_TABLE"

eval "$PSQL_CMD -c 'TRUNCATE $DB_TABLE'"
eval "$PSQL_CMD -c 'ALTER TABLE $DB_TABLE SET UNLOGGED'"

eval "unzip -p $OUTPUT_FILE | $PSQL_CMD -c '\copy $DB_TABLE from stdin with (delimiter \",\", format csv, header true)'"

eval "$PSQL_CMD -c 'ALTER TABLE $DB_TABLE SET LOGGED'"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] END IMPORTING $DB_TABLE"
