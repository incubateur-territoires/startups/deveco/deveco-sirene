#!/bin/sh
set -e

##------------------------------------------------------------------------------------------------------------------
## etablissement
##------------------------------------------------------------------------------------------------------------------
echo "[$(date '+%d/%m/%Y %H:%M:%S')] populate table etablissement"

$PSQL_CMD -c "ALTER TABLE etablissement SET UNLOGGED"

$PSQL_CMD -c "TRUNCATE etablissement"

$PSQL_CMD <11_create_agg_tables_etablissement_insert.sql

$PSQL_CMD -c "ALTER TABLE etablissement SET LOGGED;"

##------------------------------------------------------------------------------------------------------------------
## territory_etablissement_reference
##------------------------------------------------------------------------------------------------------------------

echo "[$(date '+%d/%m/%Y %H:%M:%S')] create table territory_etablissement_reference"

$PSQL_CMD -c "ALTER TABLE territory_etablissement_reference SET UNLOGGED"

$PSQL_CMD -c "TRUNCATE territory_etablissement_reference"

$PSQL_CMD <11_create_agg_tables_territory_etablissement_reference.sql

$PSQL_CMD -c "ALTER TABLE territory_etablissement_reference SET LOGGED;"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] Update all missing geoloc data in etablissements for existing territoires"
$PSQL_CMD 11_create_agg_tables_etablissement_update_geoloc.sql

echo "[$(date '+%d/%m/%Y %H:%M:%S')] Update all qpv in etablissement"
$PSQL_CMD 11_create_agg_tables_update_qpv.sql
