#!/bin/sh
set -e

case "$1" in
-t | --territory)
	territory="$2"
	;;
*)
	echo "Territory id is required"
	exit
	;;
esac

##------------------------------------------------------------------------------------------------------------------
## Update territory_etablissement_reference
##------------------------------------------------------------------------------------------------------------------

$PSQL_CMD <13_import_territory.sql
