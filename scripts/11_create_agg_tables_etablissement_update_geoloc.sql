UPDATE etablissement EL
SET
  (longitude, latitude) = (
    SELECT
      GE.x_longitude longitude,
      GE.y_latitude latitude
    FROM
      geoloc_entreprise AS GE
    WHERE
      GE.siret = EL.siret
  )
FROM
  territory T
WHERE
  EL.code_commune IN (
    SELECT
      insee_com
    FROM
      commune as C
      INNER JOIN territory T ON C.insee_com = T.commune_id
      OR (
        T.metropole_id IS NOT NULL
        AND C.com_parent = T.metropole_id
      )
      OR (
        T.epci_id IS NOT NULL
        AND C.insee_epci = T.epci_id
      )
  );
