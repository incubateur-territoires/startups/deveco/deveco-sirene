#!/bin/sh
set -e

SOURCE_URL="https://files.data.gouv.fr/geo-sirene/last/StockEtablissement_utf8_geo.csv.gz"
OUTPUT_FILE="$DATA_DIR/sirene_etablissement.csv.gz"
DB_TABLE="sirene_etablissement"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START IMPORTING $DB_TABLE"

if [ ! -f "$OUTPUT_FILE" ]; then
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD $DB_TABLE"
	wget --no-clobber --progress=bar:force:noscroll -q --show-progress $SOURCE_URL -O "$OUTPUT_FILE"
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD FINISHED $DB_TABLE"
fi

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START COPYING $DB_TABLE"

eval "$PSQL_CMD -c 'TRUNCATE $DB_TABLE'"
eval "$PSQL_CMD -c 'ALTER TABLE $DB_TABLE SET UNLOGGED'"

fields_order="siren,nic,siret,statut_diffusion_etablissement,date_creation_etablissement,tranche_effectifs_etablissement,annee_effectifs_etablissement,activite_principale_registre_metiers_etablissement,date_dernier_traitement_etablissement,etablissement_siege,nombre_periodes_etablissement,complement_adresse_etablissement,numero_voie_etablissement,indice_repetition_etablissement,type_voie_etablissement,libelle_voie_etablissement,code_postal_etablissement,libelle_commune_etablissement,libelle_commune_etranger_etablissement,distribution_speciale_etablissement,code_commune_etablissement,code_cedex_etablissement,libelle_cedex_etablissement,code_pays_etranger_etablissement,libelle_pays_etranger_etablissement,complement_adresse2_etablissement,numero_voie2_etablissement,indice_repetition2_etablissement,type_voie2_etablissement,libelle_voie2_etablissement,code_postal2_etablissement,libelle_commune2_etablissement,libelle_commune_etranger2_etablissement,distribution_speciale2_etablissement,code_commune2_etablissement,code_cedex2_etablissement,libelle_cedex2_etablissement,code_pays_etranger2_etablissement,libelle_pays_etranger2_etablissement,date_debut,etat_administratif_etablissement,enseigne1_etablissement,enseigne2_etablissement,enseigne3_etablissement,denomination_usuelle_etablissement,activite_principale_etablissement,nomenclature_activite_principale_etablissement,caractere_employeur_etablissement,longitude,latitude,geo_score,geo_type,geo_adresse,geo_id,geo_ligne,geo_l4,geo_l5"

QUERY_COPY_FROM_STDIN="gunzip -c $OUTPUT_FILE | $PSQL_CMD -c '\copy $DB_TABLE($fields_order) from stdin with (delimiter \",\", format csv, header true)'"
eval "$QUERY_COPY_FROM_STDIN"

eval "$PSQL_CMD -c 'ALTER TABLE $DB_TABLE SET LOGGED'"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] END IMPORTING $DB_TABLE"
