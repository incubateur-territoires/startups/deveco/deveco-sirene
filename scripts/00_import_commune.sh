#!/bin/sh
set -e

SOURCE_URL="https://fichiers.incubateur.tech/csv/commune.csv"
OUTPUT_FILE="$DATA_DIR/commune.csv"
DB_TABLE="commune"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START IMPORTING $DB_TABLE"

if [ ! -f "$OUTPUT_FILE" ]; then
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD $DB_TABLE"
	wget --no-clobber --progress=bar:force:noscroll -q --show-progress $SOURCE_URL -O "$OUTPUT_FILE"
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD FINISHED $DB_TABLE"
fi

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START COPYING $DB_TABLE"

eval "$PSQL_CMD -c 'truncate $DB_TABLE, epci, metropole'"

# insert all Communes
eval "$PSQL_CMD -c '\copy $DB_TABLE(typecom,insee_com,insee_dep,insee_reg,ctcd,insee_arr,tncc,ncc,nccenr,lib_com,insee_can,com_parent) FROM $OUTPUT_FILE WITH (delimiter \",\", format csv, header true)'"

# add Metropoles
eval "$PSQL_CMD -c 'INSERT INTO metropole (insee_com, lib_com) SELECT insee_com, lib_com FROM commune where insee_com IN (SELECT DISTINCT com_parent FROM commune WHERE com_parent is not null)'"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] END IMPORTING $DB_TABLE"
