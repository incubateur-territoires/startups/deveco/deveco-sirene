#!/bin/sh
set -e

SOURCE_URL="https://fichiers.incubateur.tech/csv/epci.csv"
OUTPUT_FILE="$DATA_DIR/epci.csv"
DB_TABLE="epci"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START IMPORTING $DB_TABLE"

if [ ! -f "$OUTPUT_FILE" ]; then
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD $DB_TABLE"
	wget --no-clobber --progress=bar:force:noscroll -q --show-progress $SOURCE_URL -O "$OUTPUT_FILE"
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD FINISHED $DB_TABLE"
fi

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START COPYING $DB_TABLE"

eval "$PSQL_CMD -c '\copy $DB_TABLE FROM $OUTPUT_FILE WITH (delimiter \",\", format csv, header true)'"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] END IMPORTING $DB_TABLE"
