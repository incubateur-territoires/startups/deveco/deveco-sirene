WITH
  etab AS (
    UPDATE etablissement as E
    SET
      qpv_id = QPV.gid
    FROM
      qp_metropoleoutremer_wgs84_epsg4326 as QPV
    WHERE
      ST_Contains (geom, ST_POINT (E.longitude, E.latitude))
    RETURNING
      *
  )
UPDATE territory_etablissement_reference as TER
SET
  qpv_id = etab.qpv_id
FROM
  etab
WHERE
  etab.siret = TER.siret;
