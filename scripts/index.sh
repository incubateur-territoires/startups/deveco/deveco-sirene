#!/bin/sh
set -e

DATA_DIR="${TMP_DIR}/extracted_data"
export PGPASSWORD="$POSTGRES_PASSWORD"
export PSQL_CMD="psql -U $POSTGRES_USERNAME -h $POSTGRES_HOST -p $POSTGRES_PORT -d $POSTGRES_DB"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] Create $DATA_DIR if does not exist"
[ -d "$DATA_DIR" ] || mkdir "$DATA_DIR"

. ./00_import_commune.sh
. ./01_import_epci.sh
. ./02_import_epci_commune.sh
. ./03_import_naf.sh
. ./04_import_sirene_unite_legale.sh
. ./05_import_geoloc_entreprise.sh
. ./06_import_sirene_etablissement.sh
. ./07_import_qp_metropoleoutremer.sh

. ./11_create_agg_tables.sh
. ./12_update_entreprise.sh

. ./20_clean.sh
