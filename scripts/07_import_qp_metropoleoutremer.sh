#!/bin/sh
set -e

# https://www.data.gouv.fr/fr/datasets/quartiers-prioritaires-de-la-politique-de-la-ville-qpv/
SOURCE_URL="https://www.data.gouv.fr/fr/datasets/r/6da6f4de-aed4-4ab7-8ffc-7cb7ba1e6722"
OUTPUT_FILE="$DATA_DIR/qpv.zip"
DB_TABLE="qp_metropoleoutremer_wgs84_epsg4326"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START IMPORTING $DB_TABLE"

if [ ! -f "$OUTPUT_FILE" ]; then
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD $DB_TABLE"
	wget --no-clobber --progress=bar:force:noscroll -q --show-progress $SOURCE_URL -O "$OUTPUT_FILE"
	eval "unzip $DATA_DIR/qpv.zip -d $DATA_DIR"
	eval "shp2pgsql -a -I $DATA_DIR/QP_METROPOLEOUTREMER_WGS84_EPSG4326.shp > $DATA_DIR/qpv.sql"
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD FINISHED $DB_TABLE"
fi

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START COPYING $DB_TABLE"

eval "$PSQL_CMD -c 'TRUNCATE $DB_TABLE'"
eval "$PSQL_CMD < $DATA_DIR/qpv.sql"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] END IMPORTING $DB_TABLE"
