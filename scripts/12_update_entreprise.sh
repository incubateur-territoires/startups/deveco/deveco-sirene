#!/bin/sh
set -e

echo "[$(date '+%d/%m/%Y %H:%M:%S')] update table entreprise"

$PSQL_CMD < 12_update_entreprise.sql