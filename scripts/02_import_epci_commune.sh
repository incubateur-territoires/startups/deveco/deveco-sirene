#!/bin/sh
set -e

SOURCE_URL="https://fichiers.incubateur.tech/csv/composition_epci.csv"
OUTPUT_FILE="$DATA_DIR/epci_commune.csv"
DB_TABLE="epci_commune"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START IMPORTING $DB_TABLE"

if [ ! -f "$OUTPUT_FILE" ]; then
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD $DB_TABLE"
	wget --no-clobber --progress=bar:force:noscroll -q --show-progress $SOURCE_URL -O "$OUTPUT_FILE"
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD FINISHED $DB_TABLE"
fi

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START COPYING $DB_TABLE"

eval "$PSQL_CMD -c 'truncate $DB_TABLE'"

# add the EPCI <-> Commune map
eval "$PSQL_CMD -c '\copy $DB_TABLE FROM $OUTPUT_FILE WITH (delimiter \",\", format csv, header true)'"

# use it to update the Commune table with EPCI keys
eval "$PSQL_CMD -c 'update commune set insee_epci = epci_commune.insee_epci from epci_commune where epci_commune.insee_com = commune.insee_com'"

# don't forget to mark Communes that belong to a Metropole as belonging to the same EPCI as their Metropole
eval "$PSQL_CMD -c 'update commune set insee_epci = epci_commune.insee_epci from epci_commune where epci_commune.insee_com = commune.com_parent'"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] END IMPORTING $DB_TABLE"
