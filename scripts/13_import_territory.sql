INSERT INTO
  territory_etablissement_reference (
    id,
    siret,
    territory_id,
    nom_recherche,
    etat_administratif,
    code_naf,
    libelle_naf,
    date_creation,
    categorie_juridique_courante,
    tranche_effectifs,
    code_commune,
    adresse_complete
  )
SELECT
  CONCAT(T.id, '-', EL.siret) as id,
  EL.siret as siret,
  T.id as territory_id,
  EL.nom_recherche as nom_recherche,
  EL.etat_administratif as etat_administratif,
  EL.code_naf as code_naf,
  EL.libelle_naf as libelle_naf,
  EL.date_creation as date_creation,
  CASE
    WHEN substring(CAST(EL.categorie_juridique AS VARCHAR), 1, 2) IN (
      '00',
      '21',
      '22',
      '27',
      '65',
      '71',
      '72',
      '73',
      '74',
      '81',
      '83',
      '84',
      '85',
      '91'
    ) THEN false
    ELSE true
  END as categorie_juridique_courante,
  EL.tranche_effectifs as tranche_effectifs,
  EL.code_commune as code_commune,
  EL.adresse_complete as adresse_complete
FROM
  territory T
WHERE
  T.id = $territory
  AND EL.code_commune IN (
    SELECT
      insee_com
    FROM
      commune as C
      INNER JOIN territory T ON C.insee_com = T.commune_id
      OR (
        T.metropole_id IS NOT NULL
        AND C.com_parent = T.metropole_id
      )
      OR (
        T.epci_id IS NOT NULL
        AND C.insee_epci = T.epci_id
      )
  ) ON CONFLICT
DO NOTHING;
