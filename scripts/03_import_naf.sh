#!/bin/sh
set -e

SOURCE_URL="https://www.data.gouv.fr/fr/datasets/r/7bb2184b-88cb-4c6c-a408-5a0081816dcd"
OUTPUT_FILE="$DATA_DIR/naf.csv"
DB_TABLE="naf"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START IMPORTING $DB_TABLE"

if [ ! -f "$OUTPUT_FILE" ]; then
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD $DB_TABLE"
	wget --no-clobber --progress=bar:force:noscroll -q --show-progress $SOURCE_URL -O "$OUTPUT_FILE"
	echo "[$(date '+%d/%m/%Y %H:%M:%S')] DOWNLOAD FINISHED $DB_TABLE"
fi

echo "[$(date '+%d/%m/%Y %H:%M:%S')] START COPYING $DB_TABLE"

eval "$PSQL_CMD -c 'truncate $DB_TABLE'"
eval "$PSQL_CMD -c '\copy $DB_TABLE FROM $OUTPUT_FILE WITH (delimiter \",\", format csv, header true)'"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] END IMPORTING $DB_TABLE"
