INSERT INTO
  etablissement (
    siren,
    siret,
    nom_public,
    nom_recherche,
    enseigne,
    adresse_complete,
    code_commune,
    tranche_effectifs,
    annee_effectifs,
    sigle,
    code_naf,
    libelle_naf,
    libelle_categorie_naf,
    date_creation,
    date_creation_entreprise,
    etat_administratif,
    categorie_juridique,
    type_etablissement,
    longitude,
    latitude,
    siege_social
  )
SELECT
  U.siren as siren,
  E.siret as siret,
  CASE
    WHEN U.categorie_juridique_unite_legale = '1000' THEN CASE
      WHEN U.sigle_unite_legale IS NOT NULL THEN COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '') || COALESCE(' (' || LOWER(U.sigle_unite_legale) || ')', '')
      ELSE COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '')
    END
    ELSE CASE
      WHEN U.sigle_unite_legale IS NOT NULL THEN COALESCE(LOWER(U.denomination_unite_legale), '') || COALESCE(LOWER(' (' || U.sigle_unite_legale || ')'), '')
      ELSE COALESCE(LOWER(U.denomination_unite_legale), '')
    END
  END as nom_public,
  COALESCE(LOWER(U.prenom1_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_unite_legale), '') || COALESCE(' ' || LOWER(U.nom_usage_unite_legale), '') || COALESCE(' ' || LOWER(U.sigle_unite_legale), '') || COALESCE(' ' || LOWER(U.denomination_unite_legale), '') || COALESCE(
    ' ' || LOWER(U.denomination_usuelle1_unite_legale),
    ''
  ) || COALESCE(
    ' ' || LOWER(U.denomination_usuelle2_unite_legale),
    ''
  ) || COALESCE(
    ' ' || LOWER(U.denomination_usuelle3_unite_legale),
    ''
  ) || COALESCE(' ' || LOWER(E.enseigne1_etablissement), '') || COALESCE(' ' || LOWER(E.enseigne2_etablissement), '') || COALESCE(' ' || LOWER(E.enseigne3_etablissement), '') || COALESCE(
    ' ' || LOWER(E.denomination_usuelle_etablissement),
    ''
  ) as nom_recherche,
  COALESCE(
    E.enseigne1_etablissement,
    E.enseigne2_etablissement,
    E.enseigne3_etablissement,
    E.denomination_usuelle_etablissement,
    ''
  ) as enseigne,
  COALESCE(LOWER(E.numero_voie_etablissement), '') || COALESCE(' ' || LOWER(E.type_voie_etablissement), '') || COALESCE(' ' || LOWER(E.libelle_voie_etablissement), '') || COALESCE(' ' || LOWER(E.libelle_commune_etablissement), '') || COALESCE(' ' || LOWER(E.code_postal_etablissement), '') || COALESCE(
    ' ' || LOWER(E.code_pays_etranger_etablissement),
    ''
  ) as adresse_complete,
  E.code_commune_etablissement,
  E.tranche_effectifs_etablissement,
  E.annee_effectifs_etablissement,
  U.sigle_unite_legale,
  E.activite_principale_etablissement,
  N.label_5,
  N.label_2,
  E.date_creation_etablissement,
  U.date_creation_unite_legale,
  E.etat_administratif_etablissement,
  U.categorie_juridique_unite_legale,
  CASE
    WHEN U.categorie_juridique_unite_legale = '1000' THEN 'personne_physique'
    ELSE 'personne_morale'
  END,
  E.longitude,
  E.latitude,
  E.etablissement_siege
FROM
  sirene_etablissement E
  INNER JOIN sirene_unite_legale U ON U.siren = E.siren
  LEFT JOIN naf N ON N.id_5 = E.activite_principale_etablissement
WHERE
  E.code_commune_etablissement is not NULL;
