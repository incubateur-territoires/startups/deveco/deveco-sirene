#!/bin/sh
set -e

DATA_DIR="${TMP_DIR}/extracted_data"
export PGPASSWORD="$POSTGRES_PASSWORD"
export PSQL_CMD="psql -U $POSTGRES_USERNAME -h $POSTGRES_HOST -p $POSTGRES_PORT -d $POSTGRES_DB"

echo "[$(date '+%d/%m/%Y %H:%M:%S')] Create $DATA_DIR if does not exist"
[ -d "$DATA_DIR" ] || mkdir "$DATA_DIR"
