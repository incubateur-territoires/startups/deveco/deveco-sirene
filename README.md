# deveco-sirene

Import des données SIRENE

## Description

Project constists of 2 parts. First part serves to fetch and import sirene database. The second part serves to adapt the imorted data to deveco needs.
More precisely: fill the geolocation data gaps and create aggregation tables with indexes in order to optimize the search queries.

The `main.sh` script launches `import_sirene.sh` then all the "after import scripts".
The bash script `import_sirene.sh` retrieves data from [https://www.data.gouv.fr/](https://www.data.gouv.fr/) and updates deveco database.

Tables that are concerned:

- `sirene_etablissement_geoloc`
- `sirene_unite_legale`
- `naf`
- `commune`
- `epci`
- `epci_commune`

Data sources are:

- [https://files.data.gouv.fr/geo-sirene/last/StockEtablissement_utf8_geo.csv.gz](https://files.data.gouv.fr/geo-sirene/last/StockEtablissement_utf8_geo.csv.gz)
- [https://www.data.gouv.fr/fr/datasets/r/825f4199-cadd-486c-ac46-a65a8ea1a047](https://www.data.gouv.fr/fr/datasets/r/825f4199-cadd-486c-ac46-a65a8ea1a047)
- [https://fichiers.incubateur.tech/csv/commune.csv](https://fichiers.incubateur.tech/csv/commune.csv)
- [https://fichiers.incubateur.tech/csv/epci.csv](https://fichiers.incubateur.tech/csv/epci.csv)
- [https://www.data.gouv.fr/fr/datasets/r/7bb2184b-88cb-4c6c-a408-5a0081816dcd](https://www.data.gouv.fr/fr/datasets/r/7bb2184b-88cb-4c6c-a408-5a0081816dcd)
- [https://fichiers.incubateur.tech/csv/composition_epci.csv](https://fichiers.incubateur.tech/csv/composition_epci.csv)

Before each update the current table data is truncated.
Logs are collected in `TMP_DIR/log_sirene_import.log`

## Project settings

Adjust these variables in .env:

```console
TMP_DIR=/tmp
POSTGRES_HOST=<host>
POSTGRES_DB=<db_name>
POSTGRES_USERNAME=<username>
POSTGRES_PASSWORD=<password>
POSTGRES_PORT=<port>
```

## Installation

Requires `wget` and `unzip`:

```console
apk --no-cache add wget
apk --no-cache add unzip
```

## Docker deployment

```console
docker-compose up --build
```

## Usage

To launch import

```console
./scripts/index.sh
```

Thomas Glatt <tglatt@gmail.com>
Anastasia Kryukova <anastasia.kryukova@protonmail.com>
Augustin Ragon <augustin82@proton.me>
