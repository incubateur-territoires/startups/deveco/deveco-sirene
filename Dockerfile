FROM mdillon/postgis:11-alpine

WORKDIR /tmp

# Install curl
RUN apk --no-cache add wget

# Install unzip
RUN apk --no-cache add unzip

COPY ./scripts/* ./scripts/
RUN chmod +x ./scripts/*

WORKDIR scripts

CMD ["./import_sirene.sh"]
