#!/bin/bash

if [ -z "$KUBECONFIG" ]; then
	echo "KUBECONFIG has to be set in your environment"
	exit 1
fi

set -e

COMMIT_SHA=$(git rev-parse HEAD)
IMAGE_FULLNAME="registry.gitlab.com/incubateur-territoires/startups/deveco/deveco-sirene/main:$COMMIT_SHA"

patch() {
	kubectl patch cronjob $1 --patch '{"spec":{"jobTemplate":{"spec":{"template":{"spec":{"containers":[{"name":"'"$1"'","image":"'"$IMAGE_FULLNAME"'"}]}}}}}}'
}

patch import-sirene
patch import-initial
